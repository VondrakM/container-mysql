container-mysql
===============

This project builds fully configurable [MySQL](https://www.mysql.com/) image.
The image is based on latest official [MySQL image](https://hub.docker.com/r/library/mysql/).

License
-------

This project is licensed under The [MIT License](https://opensource.org/licenses/MIT).

